# Vulnerability Scanner

## Overview

This is a vulnerability scanner written in Python 3 by Isaias Rojas. It is designed to scan a given IP address or domain and identify any known vulnerabilities.

## Requirements
- Python 3
- ```requests``` library
- ```BeautifulSoup4``` library
- ```re``` library

## Usage

To run the scanner, simply execute the script and pass the IP address or domain as an argument:

```css
python3 scanner.py [IP or domain]
```

The scanner will then scan the target and output any identified vulnerabilities.

## Limitations
The scanner only checks for known vulnerabilities and may not identify all security issues.The scanner only supports IP addresses and domains, and does not support other types of targets (e.g. files).

## Contributions

If you would like to contribute to this project, please fork the repository and create a pull request with your changes.

## Contact

If you have any questions or issues, please contact Isaias Rojas at s_isaiasr@tutanota.com.
